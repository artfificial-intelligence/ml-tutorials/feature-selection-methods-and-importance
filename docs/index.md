# 피쳐 선택 방법과 중요도 <sup>[1](#footnote_1)</sup>

> <font size="3">기계 학습 모델에 가장 관련성이 높은 피처을 선택하는 방법에 대해 알아본다.</font>

## 목차

1. [개요](./feature-selection.md#intro)
1. [피처 선택이란 무엇이며 왜 중요한가?](./feature-selection.md#sec_02)
1. [피처 선택 방법의 종류](./feature-selection.md#sec_03)
1. [일반적 피쳐 선택 기법](./feature-selection.md#sec_04)
1. [피처 선택의 성능 평가 방법](./feature-selection.md#sec_05)
1. [마치며](./feature-selection.md#summary)

<a name="footnote_1">1</a>: [ML Tutorial 13 — Feature Selection Methods and Importance](https://ai.gopubby.com/ml-tutorial-13-feature-selection-methods-and-importance-93221a37abaf?sk=9094b5037152cd07306728496b36c829)를 편역하였습니다.
